//
//  ViewController.swift
//  MultithreadingDemo
//
//  Created by Gautam Tiwari on 02/06/18.
//  Copyright © 2018 Gautam Tiwari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        print("View Did Load Start")
        
//        self.mainSerialQueue()
        
//        self.globalConcurrentQueue()
        
//        self.privateSerialQueue()
        
        
        self.privateConcurrentQueue()

        print("View Did Load Ended")
    }
    
    func mainSerialQueue(){
        
        self.whichThread(index: 0)
        //Getting main Queue
        let main = DispatchQueue.main
        
        //Never Do This
//                main.sync {
//                    print("This can never be executed")
//                    self.whichThread()
//                }
        
        
        main.async {
            
            for index in 1...5 {
                print("Main Serial Queue Execution 1 -- \(index)")
                self.whichThread(index: index)
                sleep(3)
            }
            
        }
        
        print("Test 1")
        main.async {
            print("Main Serial Queue Execution 2")
//             sleep(3)
        }

        print("Test 2")
        main.async {
            print("Main Serial Queue Execution 3")
//             sleep(3)
        }

        print("Test 3")
        main.async {
            print("Main Serial Queue Execution 4")
//             sleep(3)
        }
        
        
    }
    
    func globalConcurrentQueue(){
        
        self.whichThread(index: 0)
        //Getting global Queue
        
//        let gQueue = DispatchQueue.global()
        let gQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
        gQueue.async {
            print("Rahib -- 1")
            self.whichThread(index: 1)
        }
        
        gQueue.async {
            print("Rahib -- 2")
            self.whichThread(index: 2)
        }
        
        gQueue.async {
            print("Rahib -- 3")
            self.whichThread(index: 3)
        }
        
        gQueue.async {
            print("Rahib -- 4")
            self.whichThread(index: 4)
        }
    }
    
    
    
    
    func privateSerialQueue(){
        
        let pSerialQ = DispatchQueue.init(label: "Rahib Queue")
        pSerialQ.async {
            print("privateSerialQueue ---- 1")
        }
        
        print("1")
        
        pSerialQ.async {
            print("privateSerialQueue ---- 2")
        }
        
        print("2")
        
        pSerialQ.async {
            print("privateSerialQueue ---- 3")
        }
        
        print("3")
        
        pSerialQ.async {
            print("privateSerialQueue ---- 4")
        }
        
        print("4")
        
        pSerialQ.async {
            print("privateSerialQueue ---- 5")
        }
        
        print("5")
        
    }
    
    
    func privateConcurrentQueue(){
        
        let pConcurrentQ = DispatchQueue.init(label: "Rahib Queue", attributes: .concurrent)
        pConcurrentQ.async {
            self.whichThread(index: 1)
            print("privateConcurrentQueue ---- 1")
        }
        
        print("1")
        
        pConcurrentQ.async {
            self.whichThread(index: 2)
            print("privateConcurrentQueue ---- 2")
        }
        
        print("2")
        
        pConcurrentQ.async {
            self.whichThread(index: 3)
            print("privateConcurrentQueue ---- 3")
        }
        
        print("3")
        
        pConcurrentQ.async {
            self.whichThread(index: 4)
            print("privateConcurrentQueue ---- 4")
        }
        
        print("4")
        
        pConcurrentQ.async {
            self.whichThread(index: 5)
            print("privateConcurrentQueue ---- 5")
        }
        
        print("5")
        
        
        
    }
    
    
    
    func disptachGroupDemo(){
        
    }
    
    
    func dispatchBarrier(){
        
    }
    
    func whichThread(index: Int){
        let thread = Thread.current
        print("Thread Name On Main Queue : \(thread):  Index -- \(index)")
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

